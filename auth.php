
<?php 

session_start(); 

if(!isset($_SESSION["login"])) // if user not logined...
{

    $opt = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, // Database connection settings.
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    $dsn = "mysql:host=localhost;dbname=concurs;charset=utf8";
    $conn = new PDO($dsn,"root","",$opt) or die('Can not connect: ' . $e->getMessage());
    if(isset($_POST["conf"] )) // If confirmation data then it is registration...
    {
        if($_POST["pass"] == $_POST["conf"])
        {
            $stmt = $conn->prepare('INSERT INTO users(login,password) values(:login,:password)'); // Using PDO and safe params transfer...
            $stmt->execute(array('login' => $_POST["login"],'password'=> $_POST["pass"]));
            $_SESSION["login"] = $_POST["login"];
            header("Location: index.php"); // After registration user redirected to main page.
        }
        else
        {
            $_SESSION["bad_alert"]="Different password and confimation!!!"; // Sending error to registration page.
            header("Location: auth.php?action=register");
        }        
    }
    elseif(isset($_POST["login"]) && !isset($_POST["conf"])) 
    { 
        $stmt = $conn->prepare('SELECT * FROM users where login=:log and password=:pass'); // Checking, is user exists?
        $stmt->execute(array('log' => $_POST["login"],'pass'=> $_POST["pass"]));
        if($result = $stmt->fetch(PDO::FETCH_ASSOC)) // if yes user will be redirect to main page.
        {
            $_SESSION["login"] = $result["login"];
            header("Location: index.php");
        }
        else 
        {
            $_SESSION["bad_alert"]="Invalid login or password"; // if error, sending error to authorization page.
            header("Location: auth.php?action=login");
        }
    }

}
else // if user logined, just redirect.
{
    header("Location: index.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head> 
    <link rel="stylesheet" href="./style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
<?php if(!isset($_GET["action"])): ?> <!-- if no actions, user will see login or registration button. -->
    
    <div class="container mycont col-md-offset-4">
    <div class="row">
        <div class="col-md-4 myheader">Lorem ipsum dolor sit.</div>
    </div>
    <div class="row">
        <div class="col-md-4 mysubheader">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eum at et cumque sit obcaecati asperiores esse id. Odio, cumque reprehenderit.</div>    
    </div>
    <div class="row">
            <div class="col-md-2"><a class="btn btn-success" href="auth.php?action=login" >Login</a></div>
            <div class="col-md-2"><a class="btn btn-primary" href="auth.php?action=register" >Register</a></div>     
    </div>
<?php elseif($_GET["action"]=="login"): ?> <!-- if action login user will see login form -->
<div class="container mycont col-md-4 col-md-offset-4">
        <form action="auth.php" method="post" class="input-form">
            <?php if(isset($_SESSION["bad_alert"])): ?> <!-- if any error from prev. logining user will see it. -->
                <div class="input-group input-group-md">
                    <div class="error"><?php echo $_SESSION["bad_alert"]; unset($_SESSION["bad_alert"]); ?></div>
                </div>
            <?php endif;?>
            <div class="input-group input-group-md">
                <span class="input-group-addon"><li class="glyphicon glyphicon-user"></li></span>
                <input type="text" class="form-control" name="login" placeholder="Username">
            </div>
            <div class="input-group input-group-md">
            <span class="input-group-addon"><i class="fas fa-key"></i></span>
                <input type="text" class="form-control" name="pass" placeholder="Password">
            </div>
            <input type="submit" class="btn btn-success my" value="Enter" >
        </form>
    </div>
<?php elseif($_GET["action"]=="register"): ?> <!-- if reg. action, user will see reg. from. -->
<div class="container mycont col-md-4 col-md-offset-4">
        <form action="auth.php" method="post" class="input-form">
            <?php if(isset($_SESSION["bad_alert"])): ?> <!-- if any error from prev. reg-ing user will see it. -->
                <div class="input-group input-group-md">
                    <div class="error"><?php echo $_SESSION["bad_alert"]; unset($_SESSION["bad_alert"]); ?></div>
                </div>
            <?php endif;?>
            <div class="input-group input-group-md">
                <span class="input-group-addon"><li class="glyphicon glyphicon-user"></li></span>
                <input type="text" class="form-control" name="login" placeholder="Username">
            </div>
            <div class="input-group input-group-md">
            <span class="input-group-addon"><i class="fas fa-key"></i></span>
                <input type="text" class="form-control" name="pass" placeholder="Password">
            </div>
            <div class="input-group input-group-md">
            <span class="input-group-addon"><i class="fas fa-key"></i></span>
                <input type="text" class="form-control" name="conf" placeholder="Confirm">
            </div>
            <input type="submit" class="btn btn-primary my" value="Register" >
        </form>
</div>
<?php endif; ?>
</body>