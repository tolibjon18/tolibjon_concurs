<?php session_start();
if(!isset($_SESSION["login"])) 
    header("Location: auth.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="./style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="container mycont">
        <div class="col-md-offset-4 col-md-4 myheader">Hello, <?php echo $_SESSION["login"]; ?>!!!</div>
        <div class="col-md-offset-4 col-md-4 mysubheader">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum rem in aliquam quasi vero eligendi unde corporis perferendis doloremque repellat.</div>
        <div class="col-md-offset-4 col-md-4 "><a href="index.php?action=logout" class="btn btn-danger">Logout</a></div>
    </div>
    <?php if(isset($_GET["action"]))
           {
                session_destroy();
                header("Location: auth.php");
           } 
    ?>
</body>
</html>